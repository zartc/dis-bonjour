FROM openjdk:16-slim
WORKDIR /app
ARG  JAR_FILE=target/*-app.jar
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]
