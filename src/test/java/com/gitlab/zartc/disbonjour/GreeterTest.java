package com.gitlab.zartc.disbonjour;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


class GreeterTest {

    private static Greeter greeter;

    @BeforeAll
    public static void init() {
        greeter = new Greeter();
    }

    @ParameterizedTest(name = "dis bonjour à {0}")
    @ValueSource(strings = { "Pascal", "Gabriel", "François" })
    void disBonjour(String name) {
        assertThat(greeter.greet(name)).startsWith(" > Hello " + name);
    }
}

/* EOF */
